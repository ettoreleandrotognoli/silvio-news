import logging
from logging import Logger
from celery import shared_task
from robot import Robot
from robot.collector.shortcut import *
from robot.collector.opengraph import obj_opengraph
from news.models import RawNews, RawNewsData, OpenGraph

__logger__: Logger = logging.getLogger(__name__)


cnn = pipe(
    const('https://www.cnnbrasil.com.br/feed/'),
    get(),
    css('item'),
    foreach(dict(
        title=pipe(css('title'), as_text()),
        description=pipe(css('description'), as_text()),
        url=pipe(css('link'), as_text()),
    ))
)


@shared_task
def cnn_bot():
    with Robot() as robot:
        result = robot.sync_run(cnn)
        RawNews.objects.add_all(map(lambda args: RawNewsData(**args), result))


g1 = pipe(
    const('https://g1.globo.com/rss/g1/'),
    get(),
    css('item'),
    foreach(dict(
        title=pipe(css('title'), as_text()),
        description=pipe(css('description'), as_text()),
        url=pipe(css('link'), as_text()),
    ))
)


@shared_task
def g1_bot():
    with Robot() as robot:
        result = robot.sync_run(g1)
        RawNews.objects.add_all(map(lambda args: RawNewsData(**args), result))


def rss_collector(url: str):
    collector = pipe(
        const(url),
        get(),
        css('item,entry'),
        foreach(dict(
            title=pipe(css('title'), as_text()),
            description=pipe(css('description,summary'), as_text()),
            url=pipe(css('link'), as_text()),
        ))
    )
    return collector


@shared_task
def rss(url: str):
    collector = rss_collector(url)
    with Robot() as robot:
        result = robot.sync_run(collector)
        RawNews.objects.add_all(map(lambda args: RawNewsData(**args), result))


@shared_task
def opengraph():
    news_without_opengraph = list(RawNews.objects.all().filter(opengraph=None).values_list('url', flat=True))
    collector = pipe(
        const(news_without_opengraph),
        foreach(pipe(suppress(
            tuple(
                fn(lambda it: it),
                pipe(get(), obj_opengraph())
            ),
            tap(lambda _, it: OpenGraph.objects.add(*it)),
        )), limit=4),
    )
    with Robot() as robot:
        robot.sync_run(collector)
