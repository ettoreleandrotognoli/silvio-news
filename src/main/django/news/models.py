import logging
from logging import Logger
from django.db import models
from django.db.models import BooleanField, ExpressionWrapper, Q
from urllib.parse import urlparse
from djxp import timestamped
from typing import Iterator
from django.utils.translation import gettext_lazy as _
from dataclasses import dataclass, asdict
from robot.collector.opengraph import OpenGraph as OpenGraphData


__logger__: Logger = logging.getLogger(__name__)


@dataclass
class RawNewsData:
    url: str
    title: str
    description: str


class RawNewsQuerySet(models.QuerySet):

    def with_has_opengraph(self):
        return self.annotate(has_opengraph=ExpressionWrapper(
            ~Q(opengraph=None),
            output_field=BooleanField(),
        ))


class RawNewsManager(models.Manager.from_queryset(RawNewsQuerySet)):
    def add_all(self, news_list: Iterator[RawNewsData]):
        added = 0
        repeated = 0
        for news in news_list:
            url = urlparse(news.url)
            obj, created = self.get_or_create(
                url=news.url,
                defaults=dict(
                    hostname=url.hostname,
                    title=news.title,
                    description=news.description
                )
            )
            if created:
                added += 1
                __logger__.info('Added %s', news.url)
            else:
                __logger__.info('Repeated %s', news.url)
                repeated += 1
        return added, repeated


@timestamped
class RawNews(models.Model):

    class Meta:
        verbose_name = _('Notícia Crua')
        verbose_name_plural = _('Notícias Cruas')

    objects = RawNewsManager()

    url = models.URLField(
        unique=True,
        verbose_name=_('URL'),
        max_length=512,
    )

    hostname = models.CharField(
        max_length=256,
        verbose_name=_('Hostname'),
    )

    title = models.TextField(
        verbose_name=_('Título'),
    )

    description = models.TextField(
        verbose_name=_('Descrição'),
    )

    def __str__(self) -> str:
        return self.url


class OpenGraphQuerySet(models.QuerySet):
    pass


class OpenGraphManager(models.Manager.from_queryset(OpenGraphQuerySet)):

    def add(self, url: str, opengraph: OpenGraphData):
        news = RawNews.objects.get(url=url)
        return self.get_or_create(
            news=news,
            defaults=asdict(opengraph)
        )


@timestamped
class OpenGraph(models.Model):

    objects = OpenGraphManager()

    class Meta:
        verbose_name = _('Opengraph')
        verbose_name_plural = _('Opengraphs')

    news = models.OneToOneField(
        RawNews,
        on_delete=models.CASCADE,
        null=False,
        related_name='opengraph',
    )
    title = models.TextField(
        verbose_name=_('Título'),
        null=True,
    )
    type = models.TextField(
        verbose_name=_('Tipo'),
        null=True,
    )
    locale = models.TextField(
        verbose_name=_('Localização'),
        null=True,
    )
    description = models.TextField(
        verbose_name=_('Descrição'),
        null=True,
    )
    url = models.TextField(
        verbose_name=_('URL'),
        null=True,
    )
    site_name = models.TextField(
        verbose_name=_('Nome do site'),
        null=True,
    )
    image = models.TextField(
        verbose_name=_('Imagem'),
        null=True,
    )
    audio = models.TextField(
        verbose_name=_('Audio'),
        null=True,
    )
    video = models.TextField(
        verbose_name=_('Video'),
        null=True,
    )
    app_id = models.TextField(
        verbose_name=_('ID de Aplicação'),
        null=True,
    )

    def __str__(self) -> str:
        return '#'.join(map(str, (self.url or self.title or 'Unknown', self.pk,)))
