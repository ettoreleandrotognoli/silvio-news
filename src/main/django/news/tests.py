from django.test import TestCase
from django.urls import reverse
from django.test import LiveServerTestCase
from news.models import RawNews, RawNewsData
from django.test import Client
from django.contrib.auth.models import User
# Create your tests here.

class NewsAddAllTest(TestCase):

    def test_(self):
        news = RawNewsData(
            url='https://silvio-news.com/news/datetime/title',
            title='News Title',
            description='News Description',
        )
        self.assertEqual(RawNews.objects.all().count(), 0)
        RawNews.objects.add_all([news])
        self.assertEqual(RawNews.objects.all().count(), 1)
        RawNews.objects.add_all([news])
        self.assertEqual(RawNews.objects.all().count(), 1)
        added = RawNews.objects.all().first()
        self.assertEqual(added.title, news.title)
        self.assertEqual(added.url, news.url)
        self.assertEqual(added.description, news.description)
        self.assertEqual(added.hostname, 'silvio-news.com')



class AdminTest(TestCase):

    def test_news(self):
        root = User(username='root', is_superuser=True, is_active=True, is_staff=True)
        root.save()
        self.client.force_login(root)
        admin_models = [
            'rawnews',
            'opengraph',
        ]
        admin_views = [
            'changelist',
            'add',
        ]
        for model in admin_models:
            for view in admin_views:
                view_name = f'admin:news_{model}_{view}'
                url = reverse(view_name)
                response = self.client.get(url, follow=True)
                self.assertEqual(response.status_code, 200)
