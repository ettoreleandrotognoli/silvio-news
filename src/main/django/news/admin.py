from typing import Any
from django.contrib import admin
from django.db.models.query import QuerySet
from django.http.request import HttpRequest
from news.models import *
from django.utils.translation import gettext, gettext_lazy as _


@admin.register(RawNews)
class RawNewsAdmin(admin.ModelAdmin):

    def get_queryset(self, request: HttpRequest) -> QuerySet[Any]:
        return super().get_queryset(request).with_has_opengraph()

    @admin.display(ordering='has_opengraph', description=_('Tem Openhpragh'), boolean=True)
    def has_opengraph(self, obj):
        return obj.has_opengraph

    search_fields = (
        'title',
        'description',
        'url',
    )

    list_display = (
        'url',
        'has_opengraph',
        'title',
        'created_at',
    )

    list_filter = (
        'hostname',
    )

    readonly_fields = (
        'created_at',
        'updated_at',
        'opengraph',
    )


@admin.register(OpenGraph)
class OpenGraphAdmin(admin.ModelAdmin):

    raw_id_fields = (
        'news',
    )

    search_fields = (
        'title',
        'description',
        'url',
    )

    list_display = (
        'url',
        'title',
        'created_at',
    )

    list_filter = (
        'site_name',
        'locale',
        'type',
    )

    readonly_fields = (
        'created_at',
        'updated_at',
    )
