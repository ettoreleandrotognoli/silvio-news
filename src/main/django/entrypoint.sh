#!/bin/ash
set -e

source "/usr/src/app/.venv/bin/activate"

exec "$@"
