from django.db import models
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _
from typing import Type

FIELDS = {
    'created_at': models.DateTimeField(
        verbose_name=_('Criado em'),
        auto_now_add=True,
    ),
    'updated_at': models.DateTimeField(
        verbose_name=_('Atualizado em'),
        auto_now=True,
    ),
    'deleted_at': models.DateTimeField(
        verbose_name=_('Deletado em'),
        null=True,
        blank=True,
        default=None,
    )
}


def timestamped(model_cls=None, created_at=True, updated_at=True, deleted_at=False):
    kwargs = locals()
    fields = dict((k, v) for (k, v) in FIELDS.items() if kwargs.get(k, False))

    def decorator(model_cls: Type[models.Model]):

        for name, field in fields.items():
            field.contribute_to_class(model_cls, name=name)

        return model_cls

    if model_cls is None:
        return decorator
    else:
        return decorator(model_cls)
