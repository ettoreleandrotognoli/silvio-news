FROM python:3.11-alpine AS builder
LABEL org.opencontainers.image.authors="Ettore Leandro Tognoli <ettoreleandrotognoli@gmail.com>" \
  org.opencontainers.image.licenses="Apache-2.0" \
  org.opencontainers.image.title="OpenSchool Django"

COPY src/main/django/Pipfile /usr/src/app/Pipfile
COPY src/main/django/Pipfile.lock /usr/src/app/Pipfile.lock
WORKDIR /usr/src/app
RUN apk add build-base libpq-dev libffi-dev jpeg-dev zlib-dev
RUN python -m pip install pipenv
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --categories "packages runtime"
FROM python:3.11-alpine AS runtime
RUN apk add libpq libffi jpeg zlib
COPY --from=builder /usr/src/app/.venv /usr/src/app/.venv
COPY src/main/django/ /usr/src/app
WORKDIR /usr/src/app
ENTRYPOINT [ "/usr/src/app/entrypoint.sh" ]
ENV DEBUG=False
ENV DJANGO_ALLOW_ASYNC_UNSAFE=True

FROM runtime AS migration
CMD ["python", "manage.py", "migrate", "--no-input"]

FROM runtime AS http
EXPOSE 8080
CMD ["uvicorn", "core.asgi:application", "--host=0.0.0.0", "--port=8080", "--proxy-headers"]
