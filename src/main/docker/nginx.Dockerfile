FROM nginx:alpine
LABEL org.opencontainers.image.authors="Ettore Leandro Tognoli <ettoreleandrotognoli@gmail.com>" \
  org.opencontainers.image.licenses="Apache-2.0" \
  org.opencontainers.image.title="OpenSchool Django"


COPY ./src/main/webapp/dist /usr/share/nginx/html
COPY ./src/main/django/static /usr/share/nginx/html/static
COPY src/main/nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080
